# Curso de arduino (iniciante)

Este repositório é um apanhado de conteúdo, no modelo de um curso, para
servir de material de apoio para quem deseja aprender a programar a o
micro. Será ministrado alguns módulos, ensinando aspectos importantes
comuns a todos e quaisquer projetos com microcontroladores (na medida do
que o arduino permite realizar)

	- Programação básica

	- Entrada e saída de dados

	- Dispositivos comuns e aplicações

	- Processamento de dados

O material deste curso é livre, e portanto redistribuível sobre os termos
da mesma licença aplicada aqui. Se quiser copiar, referenciar, adaptar, ou
redistribuir, me contate, e ficarei feliz de ajudá-lo.

Allan Ribeiro
