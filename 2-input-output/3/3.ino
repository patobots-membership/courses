int ldr = A0;
int led = 11;
int brilho;

void setup(void)
{
	Serial.begin(9600);
	pinMode(ldr, INPUT);
	pinMode(led, OUTPUT);
}

void loop(void)
{
	brilho = analogRead(ldr)/4;
	analogWrite(led, brilho);
	delay(50);
}
