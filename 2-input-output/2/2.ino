int led = 13;
int chave = 7;
int estado;

void setup(void)
{
	pinMode(led, OUTPUT);
	pinMode(chave, INPUT);
}

void loop(void)
{
	delay(500);
	estado = digitalRead(chave);
	digitalWrite(led, estado);
}
