# Portas: Entrada e saída

O objetivo desta lição é tratar sobre os passos necessários para inserir e
retirar dados do arduino: configurações necessárias e ideias de
procedimentos para implementar.

## Portas

O arduino possui portas, que os programadores popularmente chamam de pinos
(são is buracos numerados na placa). As portas podem estabelecer níveis de
energia e ler esses níveis quando necessários, sejam dados analógicos, ou
digitais.

## Dados digitais

Em se tratando da computação, quase tudo que utilizamos é uma medida
digital de algo. Isso significa que em vez de existir um espectro infinito
de valores que algo pode tormar, a gente define limites para ele, criando
dois conceitos importantes: nivel lógico, e resolução.

O nivel lógico é uma lógica booleana, ou discreta, ou binária, se preferir.
Isso significa que existem apenas dois estados para um dado digital:
verdadeiro, um, alto, ligado, ou falso, zero, baixo, desligado. Essa forma
é como os computadores operam tudo a baixo nível. Mas como o controlador
faz para guardar um número gigante, se ele só entende zeros e uns?

Da mesma forma que nós: temos apenas 10 números no nosso sistema numérico,
por isso decimal: 0, 1, 2, 3, 4, 5, 6, 7, 8 e 9. Se quisermos formar um
número maior que este, adicionamos mais uma casa. É possível decompor todos
os números em potência de dez, que é a quantidade de símbolos usadas:

	1 = 1*10^0
	9 = 9*10^0
	21 = 2*10^1 + 1*10^0
	42 = 4*10^1 + 2*10^0
	109231 = 1*10^5 + 0*10^4 + 9*10^3 + 2*10^2 + 3*10^1 + 1*10^0

Com números binários é a mesma coisa, mas existem apenas dois símbolos: 0 e
1. Mas a lógica é a mesma:

	1 = 0b0001 = 1*2^0
	2 = 0b0010 = 1*2^1
	9 = 0b1001 = 1*2^3 + 1*2^0
	...

Pratique escrever alguns números, se achar interessante.

No final das contas, usamos a lógica booleana para tomar decisões: quando
algo só pode ser 1 ou 0, falso ou verdadeiro, ligado ou desligado, é mais
simples tomar decisões. Observe o seguinte exemplo:

```C
	if (a > 10)
	{
		//I will do something only if a is greater than 10
	}
	else
	{
		//I will do another thing if not
	}
```

A linha acima usa lógica booleana para responder uma pergunta e tomar uma
decisão: tomando a e 10, existem apenas duas possibilidades: ou a é maior
que 10, ou não é: se a for -100, não é maior, se a for 0, ainda também não. Se a é
10, é igual, mas ainda não é maior. Se for 10.0000...1, é
maior, e qualquer valor que for maior que isso, também é maior.

Então é facil tomar a decisão de fazer "something", ou "another thing": se
a condição de a ser maior que 10 for verdadeira, faz "something", caso
contrário, faz "another thing".

Outra forma que o arduino tem para representar isso é ligar ou desligar
alguma coisa. Ligado, para o Arduino, quer dizer 3.3V, e desligado
significa 0V, quando se trata de SAIDA (é a tensão sob a qual ele opera). Se tratando de entrada, isto é,
leitura de uma porta, qualquer tensão acima de 0 é considerado ligado. 

Para ver isso na prática, faça o seguinte teste:

### Chave de fio

Você precisará do arduino, um jumper, e algo para programar. Primeiro
escolha uma porta digital de seu interesse: qualquer uma de 2 a 12. Vamos
chamar ela de chave. Cuidado com as portas 0 e 1:
são especiais por serem usadas para comunicação do arduino. Se for usar,
não pode ligar nada nelas quando carregar o programa. A já será usada para
outra coisa nesse projeto, que é a porta padrão do LED.

Configure "chave" como entrada de dados.

A porta 13 chamaremos de led, e deve ser configurada como saída.


```C
int led = 13;
int chave = 7;
int estado;

void setup(void)
{
	pinMode(OUTPUT, led);
	pinMode(INPUT, chave);
}
```


No loop, faça o seguinte: Leia o estado da porta chave, salve em uma
variável, e compare com falso ou verdadeiro. Se for verdadeiro, deveremos
acender o led, (high na porta 13), e se for falso, apagaremos (low na porta
13).


```C
void loop(void)
{
	delay(500);
	estado = digitalRead(chave);
	if (chave == true)
	{
		digitalWrite(led, HIGH);
	}
	else
	{
		digitalWrite(led, LOW);
	}
}
```

Existem outras formas de fazer esse mesmo código. Esse é um exemplo bem
tosco, na verdade. O convido a olhar os outros exemplos.

Basicamente o que está sendo feito? Configuramos uma porta como entrada, e
lemos a tensão nela. Se essa porta tiver uma tensão alta, o led deve
acender. Se houver uma tensão baixa, o led deve apagar.

Para ver o projeto em funcionamento, use seu jumper: ligue uma extremidade
no pino 3.3V, e a outra, insira na porta "chave" quando quiser ligar, ou a
retire, quando quiser desligar.

O convido a pensar outras maneiras de escrever o mesmo código.

## Dados analógicos

Nem tudo é que é do mundo real pode ser representado usando computação, mas
podemos fazer um esforço para nos aproximar o suficiente para que a
computação seja necessária. Para isso usamos uma conversão dos chamados
dados analógicos para uma representação digital. Dados analógicos são
aqueles dados que não são exatos: Uma lâmpada pode estar ligada, ou não, ou
mesmo estar em meia luz. A velocidade de um carro pode ser 100Km/h, mas não
será exatamente 100. Com um sensor bom o suficiente, poderíamos conseguir
infinitas casas decimais e nunca teríamos o valor exato. Mas as vezes mesmo
esse valor é preciso para uma computação.

Uma forma de representar dados não exatos, ditos contínuos ou analógicos, é
pegar uma amostra dependente de uma resolução: determinar por exemplo que
não usaremos mais que 3 casas decimais (99.123) nos dará uma resolução de
0.001 de sinal. Outro exemplo é estabelecer que a tensão seja considerada
apenas de 1 em 1 volt, e teríamos 1 volt de resolução: um valor de 15.5V a
gente arredondaria para 16 ou 15, conforme a necessidade.

Para capturar esse tipo de dado, usamos a função analogRead(), e para
escrevê-la, usamos analogWrite(). A lógica é a mesma da digital,exceto que
não escrevemos, ou lemos, HIGH ou LOW em uma porta, mas sim valores entre 0
e 4095.

No próximo experimento, faremos a configuração e leitura de um dispositivio
muito peculiar chamado LDR. Note que é um sensor, portanto deve ser
considerado como entrada de dados, e por ser analógico, esquecemos a lógica
booleana, e passamos a trabalhar com numeros maiores.

A aplicação é a seguinte: o LDR retorna mais ou menos tensão conforme a luz
do ambiente ao redor. Vamos ler essa tensão, e escrever no LED, para que o
LED acenda com brilho máximo quando não houver luz, e apague quando houver
(uma lanterna automática). Veja o exemplo 3.

O circuito a ser montado é o seguinte: ligue o LDR à uma porta qualquer, e
a outra perna ao GND. Faça o mesmo para o LED, usando um resistor para não
queimar (a não ser que use o led da placa).

para fazer o teste, aplique e retire luz sobre o ldr, e veja como o led se
porta
