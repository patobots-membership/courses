int led = 13;
int milisegundos = 500;

void setup()
{
	pinMode(led, OUTPUT);
}

void loop()
{
	digitalWrite(led, HIGH);
	delay(milisegundos);
	digitalWrite(led, LOW);
	delay(milisegundos);
}
