# Programação básica

O objetivo desta lição é ensinar os principais, e mais básicos, aspectos da
progrmação de um arduino: as estruturas comuns e algumas possibilidades de
extensão.

## Lógica de programas gerais

Um programa consiste, basicamente, na descrição de um processo, que precisa
ser realizado de forma computacional, para um computador. O computador é
extremamente obediente, então se teu programa não funcionar, a culpa é
sempre sua: você não explicou direito.


### Variáveis

Tudo que é processado gera informação, e essa informação precisa ser
armazenada de alguma forma. Para isso utilizamos variáveis: pequenos
pedaços de memória que recebem um nome, e podem ser chamadas para consultar
o valor que elas guardam.

Para utilizar as variáveis, primeiro precisamos dizer que elas existem:
fazer sua declaração. abaixo tem-se um exemplo da declaração de variável

```C
	int a;
	int b, c;
	int abcd;
	int a2, a_2, A2, A_2;
	int qualquer_nome_que_eu_queira_dar_que_seja_continuo_sem_acento_e_nao_começa_por_numero;
```

Existem algumas regras para estabelecer o nome de uma variável: primeiro,
precisa ser apenas 1 nome: não podemos usar espaço, e chamar "int minha
variável", bem como não se pode usar: acentos, símbolos como parenteses,
chaves e colchetes, @, \*, barra, contrabarra, e assim por diante. Outra
coisa importante é que ela não precisa ter significado algum, mas facilita
a leitura se for um nome "contextualizado".

Uma última coisa EXTREMAMENTE importante: C é case sensitive, ou seja,
letras maíusculas e minúsculas são diferentes. Isso significa que
"Albergue" e "albergue" são duas variáveis diferentes.

```C
	int a, i;
	int contador;
```

No exemplo acima, "a" pode ser usado para qualquer coisa. E contador também,
mas o nome "contador" deixa subentendido que esta variável será usada para
contar alguma coisa. Outro ponto importante, é a variável "i" representa
justamente isso para programadores: poderíamos usar "contador", mas
prefirimos "i". é mais curto assim, e é um padrão. Todos usam.

Outra coisa importante é a palavrinha "int". Assim como na matemática, os
números tem domínios: naturais (inteiros e sem sinal), inteiros (inteiros e
com sinais), inteiros muito grandes (esse é extra), racionais (inteiros, não
inteiros, negativos e positivos), e decimais muito "precisos" (são os
reais, mas com casas limitadas pela memória disponível). Para todos estes
números existem um "tipo" definido:

	void - Significa nulo: não existe um dado
	unsigned - significa sem sinal
	long - significa "longo": mais memória para guardar numeros maiores
	char, int, float - caractere, inteiro e decimal, respectivamente

Para que unsigned? 
Em alguns casos é possível se abster de
usar sinal num número, ganhando um pouco mais de memória (se o controlador
sabe que sempre será positivo, ele não precisa lembrar que o sinal existe:
mais espaço para casas numéricas).

Esses tipos são os mesmos recebidos e devolvidos por funções.

### Funções

As funções são agregados de instruções que usamos para explicitar o que
queremos fazer. Imagine que você queira jogar videogame: tem uma série
de passos que são necessários serem realizados para executar essa tarefa.
Nós não as percebemos, mas fazemos inconscientemente:

	- Precisamos ter um videogame

	- Precisamos ter um jogo

	- O videogame precisa estar funcional

	- O jogo precisa estar funcional

	- Precisamos ligar o videogame à TV

	- Precisamos por o jogo no videogame

	- ...

Esses passos são praticamente o mesmo para os computadores, em qualquer
procedimento computacional: verificação das informações necessárias,
processamento das informações, e devolutiva do resultado do processamento.
Tudo isso precisa ser muito bem detalhado, para que ele realize a tarefa de
forma esperada, do contrário, haverão BUGS, que são comportamentos
indesejados de um programa.

As funções (Sim. o mesmo nome da matemática), consistem em 3 coisas
importantes de se saber, quando uma é criada: o nome dela, o que ela
recebe, e o que ela retorna.

Funções, na matemática, são praticamente a mesma coisa: y = f(x), tal que x
e y pertencem aos inteiros: a função tem o nome f, recebe uma variável x de
entrada, e retorna uma variável y de saída. Abaixo temos um exemplo de
declaração de função em C

```C
	int f(int x)
	{
		//Eu não faço nada, ainda...
	}
```

É uma função que segue o mesmo exemplo da matemática: se chama f, recebe um
inteiro x, e retorna outro inteiro y.

Existem duas funções muito importantes com a qual precisamos nos preocupar no
Arduino: a setup, e a loop:

```C
	void setup(void)
	{
		//...
	}

	void loop(void)
	{
		//...
	}
```

### Configuração - void setup(void)

Esta etapa estabelece configurações do hardware que precisam estar prontas
para o programa executar com sucesso: portas, variáveis (se possível e
necessário), e algumas rotinas de teste devem ser executadas nessa etapa.
Mas vamos deixar para falar disso em "2-input-output". Por enquanto você
precisa saber que esta função, por ser uma pré-configuração para as tarefas
que serão realizadas, só será executada UMA VEZ, quando o controlador
ligar, então não dá pra fazer nosso procedimento aqui dentro. Apenas se
preparar para o fazer. Abaixo temos um exemplo de configuração para um
programa que pisca um led

```
	void setup()
	{
		pinMode(13, OUTPUT);
	}
```

### Execução - void loop(void)

Nessa etapa estamos livres, e prontos para fazer o procedimento que
queremos. Quaisquer cálculos, ações, decisões e respostas devem ser
executadas aqui dentro. É importante saber que essa função será rodada
infinitamente enquanto o Arduino estiver ligado, por isso se chama loop.
Que tipo de procedimentos devemos realizar? imagine que queiramos piscar um
LED. É uma atividade repetitiva, então é ideal para para executar aqui:
vamos pensar nos passos (na verdade eu vou te dizer): Piscar consiste em
ligar, aguardar um tempo, apagar, guardar um tempo, ligar, guardar um
tempo... você entendeu. Então para propor um algoritmo repetitivo, podemos
descrever assim:

	CONFIGURAR:

	- Configurar o LED

	REPETIR:

	- Acender o LED

	- Esperar um tempo

	- Apagar o LED

	- Esperar um tempo

Perceba que a etapa "REPETIR" executa de cima para baixo, e quando chega ao
fim, retorna ao começo, portanto, vai na loop. Segue o exemplo abaixo:

```C
	void loop()
	{
		digitalWrite(13, HIGH);
		delay(500);
		digitalWrite(13, LOW);
		delay(500);
	}
```

Chegando na ultima linha, o programa aguardará 500 milissegundos antes de
retornar ao início do loop: o led acende, fica meio segundo aceso, apaga,
fica meio segundo apagado (repete), acende, fica meio segundo aceso...

